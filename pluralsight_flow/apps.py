from django.apps import AppConfig


class PluralsightFlowConfig(AppConfig):
    name = 'pluralsight_flow'
