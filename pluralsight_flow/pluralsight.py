import logging

import requests, arrow
from .models import Team, Author, Repository, Commit
from django.conf import settings

BASE_URL = "https://flow.pluralsight.com/v3/customer/core/"

logger = logging.getLogger(__name__)


def get_request(path: str, params=None) -> list:
    if params is None:
        params = {}
    response = []
    headers = {"Authorization": f"Bearer {settings.FLOW_API_KEY}"}
    has_next = True
    url = BASE_URL + path
    while has_next:
        r = requests.get(url, headers=headers, params=params)
        response_chunk = r.json()
        response = response + response_chunk["results"]
        if response_chunk["next"]:
            url = response_chunk["next"]
        else:
            has_next = False
    return response


def import_teams():
    response = get_request("teams/")
    for t in response:
        team, created = Team.objects.get_or_create(id=t["id"], name=t["name"])
        if created:
            logger.info(f"Creating team %s", t["name"])


def import_repositories():
    response = get_request("repos/")
    for t in response:
        repository, created = Repository.objects.get_or_create(
            id=t["id"], defaults={"name": t["name"]}
        )
        if created:
            logger.info(f"Creating repository %s", t["name"])


def import_users():
    users = get_request(f"users/")
    for u in users:
        author, created = Author.objects.get_or_create(
            id=u["id"], defaults={"name": u["name"], "email": u["email"]}
        )

        author.teams.clear()
        for t in u["teams"]:
            team = Team.objects.get(id=t["id"])
            author.teams.add(team)

        if created:
            logger.info(f"Creating user %s", u["name"])


def import_commits(days: int = 1):
    """
    Import commits of the past 24h
    :return:
    """
    params = {
        "committer_date__gte": str(
            arrow.utcnow().shift(days=-days).format("YYYY-MM-DD HH:mm:ss")
        ),
        "smart_dedupe": True,
    }
    commits = get_request("commits/", params=params)

    cnt = 0
    for c in commits:
        author = Author.objects.get(id=c["apex_users"][0])
        repository = Repository.objects.get(id=c["repo_id"])
        commit, created = Commit.objects.update_or_create(
            id=c["id"],
            defaults={
                "created_at": arrow.get(
                    c["author_date"], "YYYY-MM-DDTHH:mm:ss"
                ).datetime,
                "haloc": c["haloc"],
                "hunks": c["hunks"],
                "new_work": c["new_work"],
                "legacy_refactor": c["legacy_refactor"],
                "churn": c["churn"],
                "files": c["files"],
                "insertions": c["insertions"],
                "deletions": c["deletions"],
                "risk": c["risk"],
                "impact": c["impact"],
                "message": c["message"],
                "is_merge": c["is_merge"],
                "is_trivial": c["is_trivial"],
                "is_outlier": c["is_outlier"],
                "outlier_reason": c["outlier_reason"],
                "logical_code": c["logical_code"],
                "author": author,
                "repository": repository,
            },
        )
        if created:
            cnt += 1
    logger.info(f"Created %i commits", cnt)
