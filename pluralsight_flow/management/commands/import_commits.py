from django.core.management.base import BaseCommand
from pluralsight_flow.pluralsight import import_teams, import_commits, import_repositories, import_users
from pluralsight_flow.models import Commit, Author, Team


class Command(BaseCommand):
    help = "Import commits"

    def add_arguments(self, parser):
        parser.add_argument("--days", type=int, default=1)

    def handle(self, days, *args, **options):
        import_teams()
        import_users()
        import_repositories()
        import_commits(days=days)
