from django.db import models


class Repository(models.Model):
    id = models.BigIntegerField(primary_key=True, null=False)
    name = models.CharField(max_length=50)


class Team(models.Model):
    id = models.BigIntegerField(primary_key=True, null=False)
    name = models.CharField(max_length=50)


class Author(models.Model):
    id = models.BigIntegerField(primary_key=True, null=False)
    name = models.CharField(max_length=150)
    email = models.CharField(max_length=150)
    teams = models.ManyToManyField(Team)


class Commit(models.Model):
    id = models.BigIntegerField(primary_key=True, null=False)
    created_at = models.DateTimeField(null=True)
    haloc = models.IntegerField(null=True)
    hunks = models.IntegerField(null=True)
    new_work = models.IntegerField()
    legacy_refactor = models.IntegerField()
    churn = models.IntegerField()
    files = models.IntegerField()
    insertions = models.IntegerField()
    deletions = models.IntegerField()
    risk = models.FloatField()
    impact = models.FloatField()
    is_merge = models.BooleanField(null=True)
    is_trivial = models.BooleanField(null=True)
    is_outlier = models.BooleanField(null=True)
    outlier_reason = models.IntegerField(null=True)
    logical_code = models.IntegerField(null=True)
    message = models.TextField()
    author = models.ForeignKey(Author, on_delete=models.CASCADE, null=True)
    repository = models.ForeignKey(Repository, on_delete=models.CASCADE, null=True)
