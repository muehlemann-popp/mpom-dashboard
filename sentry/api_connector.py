import logging
from pprint import pprint

import requests, arrow
from django.conf import settings

from sentry.models import Project, Issue

BASE_URL = 'https://sentry.io/api/0/'

logger = logging.getLogger(__name__)


def get_request(path: str, params=None) -> list:
    if params is None:
        params = {}
    headers = {'Authorization': f'Bearer {settings.SENTRY_API_KEY}'}
    url = BASE_URL + path
    r = requests.get(url, headers=headers, params=params)
    return r.json()


def import_projects():
    response = get_request('projects/')
    for p in response:
        project, created = Project.objects.update_or_create(id=int(p['id']),
                                                            defaults={
                                                            "slug":p['slug'],
                                                            "name":p['name'],
                                                            "organization":p['organization']['name'],
                                                            "organization_slug":p['organization']['slug']
                                                            }
                                                            )
        if created:
            logger.info("Imported project %s", p['name'])


def import_issues(days=1):
    for p in Project.objects.all():
        created_count = 0
        response = get_request(f"projects/{p.organization_slug}/{p.slug}/issues/")
        created = False
        for i in response:
            issue, created = Issue.objects.update_or_create(id=i['id'],
                                                            defaults={
                                                            "project":p,
                                                            "title":i['title'][0:250],
                                                            "count":i['count'],
                                                            "users":i['userCount'],
                                                            "is_resolved":i['status'] == 'resolved',
                                                            "first_seen":arrow.get(i['firstSeen']).datetime,
                                                            "last_seen":arrow.get(i['lastSeen']).datetime
                                                            })
            if created:
                created_count +=1
        if created:
            logger.info("Project %s: Created %i events", p.name, created_count)
