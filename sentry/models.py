from django.db import models

# Create your models here.
class Project(models.Model):
    id = models.BigIntegerField(primary_key=True, null=False)
    name = models.CharField(max_length=50)
    slug = models.CharField(max_length=50, unique=True)
    organization = models.CharField(max_length=50, null=True)
    organization_slug = models.CharField(max_length=50, null=True)

class Issue(models.Model):
    id = models.BigIntegerField(primary_key=True, null=False)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    title = models.CharField(max_length=250, null=True)
    count = models.IntegerField(default=0)
    users = models.IntegerField(default=0)
    is_resolved = models.BooleanField(null=True)
    last_seen = models.DateTimeField()
    first_seen = models.DateTimeField()

