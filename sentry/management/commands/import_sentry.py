from django.core.management.base import BaseCommand

from sentry.api_connector import import_projects, import_issues
from sentry.models import Project, Issue


class Command(BaseCommand):
    help = "Import Sentry"

    def add_arguments(self, parser):
        parser.add_argument("--days", type=int, default=1)

    def handle(self, days, *args, **options):
        Project.objects.all().delete()
        Issue.objects.all().delete()
        import_projects()
        import_issues(days=days)
