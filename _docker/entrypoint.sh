#!/bin/bash
set -e

python ./manage.py migrate --no-input
python ./manage.py import_commits --days=1
python ./manage.py import_sentry --days=1
python ./manage.py import_tempo --days=1
