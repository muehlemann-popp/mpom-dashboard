FROM python:3.7-slim-stretch

WORKDIR /code
CMD ["/bin/entrypoint.sh"]
ADD ./_docker/entrypoint.sh /bin/

ADD . ./
RUN pip3 install -r requirements.txt

ENTRYPOINT [ "/bin/entrypoint.sh" ]
