import logging
from pprint import pprint

import requests, arrow
from django.conf import settings

from tempo.models import User, Project

BASE_URL = 'https://m-p.atlassian.net/rest/api/3/'

logger = logging.getLogger(__name__)

class DashboardException(Exception):
    pass

def get_request(path: str, params=None) -> list:
    if params is None:
        params = {}
    url = BASE_URL + path
    r = requests.get(url,
                     auth=(settings.JIRA_USERNAME, settings.JIRA_API_KEY),
                     params=params)
    if r.status_code == 200:
        return r.json()
    else:
        raise DashboardException(f"{r.status_code} {r.content}")

def get_paginated_request(path: str, params=None) -> list:
    if params is None:
        params = {}
    response = []
    is_last = False
    params['startAt'] = 0
    url = BASE_URL + path
    while not is_last:
        r = requests.get(url,
                         auth=(settings.JIRA_USERNAME, settings.JIRA_API_KEY),
                         params=params)
        response_chunk = r.json()
        response = response + response_chunk['values']
        params['startAt'] = len(response)
        is_last = response_chunk['isLast']
    return response

def import_users():
    response = get_request(f"users/search")
    for entry in response:
        if entry['accountType'] != 'atlassian':
            continue
        if not entry['active']:
            continue

        for u in response:
            user, created = User.objects.update_or_create(id=u['accountId'],
                                                          defaults= { 'displayName' : u['displayName'] })
            if created:
                logger.info("Imported user %s", user.displayName)


def import_projects():
    response = get_paginated_request(f"project/search")
    for p in response:
        project, created = Project.objects.update_or_create(id=int(p['id']),
                                                            defaults={
                                                              "icon":p['avatarUrls']['48x48'],
                                                              "name":p['name'],
                                                              "key":p['key']
                                                            }
                                                      )
        if created:
            logger.info("Imported project %s", project.name)

