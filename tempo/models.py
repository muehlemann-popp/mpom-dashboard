import uuid

from django.db import models


class User(models.Model):
    id = models.CharField(primary_key=True, max_length=100, editable=False)
    displayName = models.CharField(max_length=50)


class Project(models.Model):
    id = models.IntegerField(primary_key=True, null=False)
    name = models.CharField(max_length=50)
    icon = models.CharField(max_length=200)
    key = models.CharField(max_length=10)


class Worklog(models.Model):
    id = models.IntegerField(primary_key=True, null=False, help_text="Tempo Worklog Id")
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    time_spent_seconds = models.IntegerField(default=0)
    billable_seconds = models.IntegerField(default=0)
    start_datetime = models.DateTimeField(null=True)
    created_at = models.DateTimeField(null=True)
    updated_at = models.DateTimeField(null=True)
    description = models.TextField()
    issue = models.CharField(null=True, max_length=30, db_index=True)
