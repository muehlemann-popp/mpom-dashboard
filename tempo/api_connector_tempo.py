import logging
from pprint import pprint
import re

import requests, arrow
from django.conf import settings

from tempo.models import Worklog, Project, User

BASE_URL = "https://api.tempo.io/core/3/"

logger = logging.getLogger(__name__)


def get_request(path: str, params=None) -> list:
    if params is None:
        params = {}
    response = []
    headers = {"Authorization": f"Bearer {settings.TEMPO_API_KEY}"}
    has_next = True
    url = BASE_URL + path
    while has_next:
        r = requests.get(url, headers=headers, params=params)
        response_chunk = r.json()
        response = response + response_chunk["results"]
        if "next" in response_chunk["metadata"]:
            url = response_chunk["metadata"]["next"]
        else:
            has_next = False
    return response


def import_worklogs(days=1):
    cnt = 0
    response = get_request(
        f"worklogs/",
        {
            "from": arrow.now().shift(days=-days).format("YYYY-MM-DD"),
            "to": arrow.now().format("YYYY-MM-DD"),
        },
    )
    for e in response:
        i = re.search("(\\w+)\\-", e['issue']['key'])
        project = Project.objects.get(key=i.group(1))
        user = User.objects.get(id=e["author"]["accountId"])

        if "startDate" in e:
            start_datetime = arrow.get(
                e["startDate"] + " " + e["startTime"], "YYYY-MM-DD HH:mm:ss"
            ).datetime
        else:
            start_datetime = None

        item, created = Worklog.objects.update_or_create(
            id=int(e["tempoWorklogId"]),
            defaults={
                "project":project,
                "user":user,
                "time_spent_seconds":int(e["timeSpentSeconds"]),
                "billable_seconds":int(e["billableSeconds"]),
                "start_datetime":start_datetime,
                "created_at":arrow.get(e["createdAt"], "YYYY-MM-DDTHH:mm:ssZ").datetime,
                "updated_at":arrow.get(e["updatedAt"], "YYYY-MM-DDTHH:mm:ssZ").datetime,
                "description":e["description"],
                "issue":e["issue"]['key']
            }
        )
        if created:
            cnt += 1

    logger.info("Imported %i worklog items", cnt)
