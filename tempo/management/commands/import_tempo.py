from django.core.management.base import BaseCommand

from tempo.api_connector_tempo import import_worklogs
from tempo.api_connector_jira import import_users, import_projects


class Command(BaseCommand):
    help = "Import Tempo Worklogs"

    def add_arguments(self, parser):
        parser.add_argument("--days", type=int, default=1)

    def handle(self, days, *args, **options):
        import_users()
        import_projects()
        import_worklogs(days=days)


